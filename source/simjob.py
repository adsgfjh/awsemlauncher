import os
import subprocess as sp
import shutil
from abc import ABCMeta, abstractmethod


class SimulationLauncher:
    __metaclass__ = ABCMeta

    pbs_template = '''#!/bin/bash
        #PBS -N %s
        #PBS -j oe
        #PBS -q alphaq
        #PBS -l %s
        
        cd $PBS_O_WORKDIR
        
        %s
        '''

    @abstractmethod
    def modify(self, ijob, ifolder):
        # do whatever operations here such as modify parameters in the input file
        pass

    def run(self, init_runid=0):
        # create project folder
        project_folder = os.path.join(self.work_dir, self.project_name)
        if self.run_id == init_runid:
            self.create_folder(project_folder, overwrite=self.overwrite)

        for ijob in range(self.njobs):
            # create work folder
            ifolder = os.path.join(project_folder, self.project_name + '_%02d' % ijob, 'run_%02d' % self.run_id)
            self.create_folder(ifolder, overwrite=self.overwrite)
            # copy files to work folder & modify if necessary
            self.prepare_file_in_workdir(ifolder, init_runid)
            # modify parameter files, for example
            self.modify(ijob, ifolder)
            # create submit file
            self.create_pbs_file(ijob, ifolder)
            # submit
            self.submit(ifolder, self.submit_file, self.test_submit)

    def prepare_file_in_workdir(self, ifolder, init_runid):
        for ifile in self.copy_files:
            if os.path.isdir(ifile):
                self.copy_from_file_to(os.path.join(self.current_dir, ifile), '*', ifolder)
            else:
                self.copy_from_file_to(self.current_dir, ifile, ifolder)
        if self.run_id > init_runid:
            self.restart_setting(ifolder)
        else:
            pass

    def restart_setting(self, ifolder):
        prev = ifolder.replace('run_%02d' % self.run_id, 'run_%02d' % (self.run_id - 1))
        restart = os.path.join(prev, '%s' % self.restart_filename).replace('/', '\/')
        if not os.path.isfile(restart):
            raise OSError, 'restart file not found: %s' % restart
        self.infile_addline('read_data', 'read_restart  ' + restart,  ifolder, self.input_file)
        self.infile_comment('read_data', ifolder, self.input_file)
        self.infile_comment('minimize', ifolder, self.input_file)

    def create_pbs_file(self, ijob, ifolder):
        # pbs nodes
        pbs_nodes = self.modify_pbs_nodes(self.submit_nodes, self.submit_ppn)
        # pbs cmd
        pbs_cmd = self.modify_pbs_cmd(ifolder, self.submit_cmd)
        # create pbs file in ifolder
        self.submit_file = os.path.join(ifolder, self.project_name + '.pbs')
        job_name = self.project_name + '_' + str(ijob)
        with open(self.submit_file, 'w') as f:
            f.write(self.pbs_template % (job_name, pbs_nodes, pbs_cmd))

    def modify_pbs_cmd(self, ifolder, cmd):
        pre_cmd = 'cd %s && ' % ifolder
        return pre_cmd + cmd

    def modify_pbs_nodes(self, node, ppn):
        """ node and ppn could be either int or list of int
            e.g.
            PBS -l nodes=node2:ppn=40  <= node = 2, ppn = 40
            PBS -l nodes=node2:ppn=40+node1:ppn=20  <= node = [2, 1], ppn = [40, 20]
            """
        pre_s = 'nodes='
        s = lambda node, ppn: 'node%s:ppn=%s' % (node, ppn)
        if isinstance(node, int) & isinstance(ppn, int):
            pbs_nodes = pre_s + s(node, ppn)
        elif isinstance(node, list) & isinstance(ppn, list):
            if len(node) != len(ppn):
                raise ValueError('len(node) must equal len(ppn).')
            l_ = []
            for i in range(len(node)):
                l_.append(s(node[i], ppn[i]))
            pbs_nodes = pre_s + '+'.join(l_)
        else:
            raise TypeError('node and ppn must be either integers or list of integers.')
        return pbs_nodes

    def submit(self, ifolder, submit_file, test):
        """ Go to {run_dr} and then submit """
        cmd = 'cd %s && qsub %s' % (ifolder, submit_file)
        if test:
            self._print('test submit: ')
            self._print(cmd)
        else:
            self.run_subprocess(cmd)

    @staticmethod
    def search_in_file(target_str, filename):
        """ Search target_str in filename and return the linenumber """
        if not os.path.exists(filename):
            raise OSError, '%s not found.' % filename
        cmd = 'grep -nr \'%s\' %s' % (target_str, filename)
        out = SimulationLauncher.run_subprocess(cmd)
        if not out:
            print('Warning: could not find \'%s\' in file \'%s\'' % (target_str, filename))
            return -1
        else:
            return int(out.split(':')[0])

    @staticmethod
    def infile_comment(target_line, path, filename=None, uncomment=False):
        """ Comment out target_line """
        f = path if filename is None else os.path.join(path, filename)
        if uncomment:
            target_line = '#' + target_line
        line_number = SimulationLauncher.search_in_file(target_line, f)
        if line_number == -1:
            return
        if uncomment:
            cmd = "sed -i '%ss/%s/%s/' %s" % (line_number, target_line, target_line[1:], f)
        else:
            cmd = "sed -i '%ss/%s/#%s/' %s" % (line_number, target_line, target_line, f)
        SimulationLauncher.run_subprocess(cmd)

    @staticmethod
    def infile_addline(below, add, path, filename=None):
        """ Add a new line below target_line with target_line """
        f = path if filename is None else os.path.join(path, filename)
        line_number = SimulationLauncher.search_in_file(below, f)
        if line_number == -1:
            return
        cmd = "sed -i '%sa\%s' %s" % (line_number, add, f)
        SimulationLauncher.run_subprocess(cmd)

    @staticmethod
    def infile_replace(target_line, replace, path, filename=None):
        """ Replace the target_line with replace """
        f = path if filename is None else os.path.join(path, filename)
        line_number = SimulationLauncher.search_in_file(target_line, f)
        if line_number == -1:
            return
        cmd = "sed -i '%ss/%s/%s/' %s" % (line_number, target_line, replace, f)
        SimulationLauncher.run_subprocess(cmd)

    @staticmethod
    def run_subprocess(cmd):
        proc = sp.Popen(cmd, shell=True, stdout=sp.PIPE)
        pout, perr = proc.communicate()
        if perr:
            print(perr)
        return pout

    @staticmethod
    def create_folder(folder_path, overwrite=False):
        if os.path.exists(folder_path):
            if overwrite:
                shutil.rmtree(folder_path)
                os.makedirs(folder_path)
            else:
                print('Stop to prevent potential overwriting.')
                raise OSError('%s already exists.' % folder_path)
        else:
            os.makedirs(folder_path)

    @staticmethod
    def copy_from_file_to(path_from, files, path_to):
        if not isinstance(files, (str, list)):
            raise TypeError('Input files must be a string or a list of strings.')
        if isinstance(files, str):
            files = [files]
        for ifile in files:
            copy_ifile = os.path.join(path_from, str(ifile))
            cmd = 'cp -r %s %s' % (copy_ifile, path_to)
            SimulationLauncher.run_subprocess(cmd)

    def _print(self, msg, verbose=None):
        v = self.verbose if verbose is None else verbose
        if v:
            print(msg)
