#!/usr/bin/env python

import os
import sys
sys.path.append('/work/shared/awsemlauncher/source')
from simjob import SimulationLauncher
import random

class MyProject(SimulationLauncher):

    current_dir = os.getcwd()
    
    def __init__(self, test=True):
        """ Settings and parameters for the project """
        
        # == general setting ==
        self.overwrite = True  # whether to overwrite project folder if it already exist
        self.verbose = True  # whether to print information
        self.test_submit = test  # to control whether to actually submit jobs
        
        # == details ==
        self.work_dir = '/work/ace'  # the location of your project will be
        self.project_name = 'test'  # will create a project folder named as this in the work_dir
        self.njobs = 3  # max 99, number of independent simulations
        self.run_id = 0  # to control restart run
        self.restart_filename = ''  # only used when run_id > 0
        
        # == parameter files/folders in work dir ==
        # assume these files/folders are in the same forlder as this notebook/script
        self.copy_files = ['test.txt', 'unBias_tmpFolder']  # copy files/folders to each folder for njobs
        
        # == execution ==
        self.exe_path = '/work/shared/awsemmd/lmp_serial_g++_12182019'  # the location of your executable
        self.input_file = 'd1_Modeller_6n3c_12.in'  # the input file for the executable
        ## restart run will read this input file and modify it for a continued simulation
        self.submit_cmd = self.exe_path + ' < ' + self.input_file
        ## cd to the work dir will be added automatically
        
        # == cluster setting ==
        self.submit_nodes = 1  # [1, 3]
        self.submit_ppn = 2  # [10, 40]
    
    
    def modify(self, ijob, ifolder):
        """ Modify parameters for different simulation runs """
        # These are example usage for the three functions
        
        self.infile_addline('banana', 'im a line below banana', ifolder, filename='test.txt')
        
        self.infile_replace('banana', 'apple'+str(ijob), ifolder, filename='test.txt')
        r = random.randint(1, 1e8)
        self.infile_replace('velocity all create 300.0 2349852',
                            'velocity all create 300.0 %s' % r, ifolder, filename=self.input_file)
                            
        self.infile_comment('im a line below banana', ifolder, filename='test.txt')


if __name__ == '__main__':
    test = False
    if len(sys.argv) != 1:
        if 'test' in sys.argv[1:]:
            test = True

    myproject = MyProject(test=test)
    myproject.run()
